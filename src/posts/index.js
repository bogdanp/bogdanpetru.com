import whatsThis from './whats-this';
import aboutOptiflow from './about-optiflow';

export default {
  'whats-this': whatsThis,
  'about-optiflow': aboutOptiflow,
};
